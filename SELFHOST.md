## Self-hosting Guide

**You don't have to self host.** You can simply use one of the IPs on README.md and it'll have the same functionality. This is mostly for people who want to block more/less addresses or for people who have issues accessing dns servers outside of their networks due to their ISPs.

- Decide on which DNS server to use (between BIND9, dnsmasq, pihole, openwrt, Windows Server DNS Services (not available on consumer Windows installs, only on Windows Server)). Dnsmasq is the easiest to use but BIND9 is designed for bigger scale deployments, though there's some cases where dnsmasq was deployed on a big scale and did well. I prefer BIND9.
- Decide on if you'll trust me with wifi check bypass or not. If you do trust me, don't bother with getting an HTTP server.
- If you will get an HTTP server, pick between nginx and apache2, both configs are available here..

- Install your preferred DNS software on a server, start and enable it (example: `apt install bind9`, `systemctl enable --now bind9`).
- Make sure that the port 53 is both open and accessible (you can use `nmap <IP>` on a different machine)

Only if you chose to not trust me with wifi check bypass:

- Install a web server (nginx or Apache) on a server, start and enable it (example: `apt install nginx`, `systemctl enable --now nginx`)
- Make sure that the port 80 is both open and accessible (you can use `nmap <IP>` on a different machine)

### BIND9

This part of guide is only for people who chose to go with BIND9. Please do not follow this section if you chose to use dnsmasq, thank you!

- Download the `BIND9/named.conf.options` file from this repo, and replace `/etc/bind/named.conf.options` on the server with that file
- Optional: Change the forward DNS IPs on `/etc/bind/named.conf.options` (if you don't trust lavadns or 1.1.1.1)
- Make a directory at `/etc/bind/zones`
- Download all the files in `BIND9/zones/` folder of the repo to `/etc/bind/zones` on server
- Optional, for people who didn't trust with wifi check bypass: edit the files `/etc/bind/zones/nintendo.net` and `/etc/bind/zones/nintendowifi.net`, replace `95.216.149.205` with your own IP (if you intend to make the service accessible outside of your home network use your public IP, if not use your private IP)
- Make sure that bind user can access `/etc/bind/zones` folder (you may need to tinker with permissions here)
- Restart BIND9 (`# systemctl restart bind9`)

### dnsmasq

This part of guide is only for people who chose to go with dnsmasq. Please do not follow this section if you chose to use BIND9, thank you!

- Download the `dnsmasq/dnsmasq.conf` file from this repo, and replace `/etc/dnsmasq.conf` on the server with that file.
- Put your correct interface on the top line.
- Optional, for people who didn't trust with wifi check bypass: edit lines with `nintendo.net` and `nintendowifi.net`, replace `95.216.149.205` with your own IP (if you intend to make the service accessible outside of your home network use your public IP, if not use your private IP)
- Restart dnsmasq (`# systemctl restart dnsmasq`)

### Unbound

This part of guide is only for people who chose to go with Unbound. Please do not follow this section if you chose to use BIND9 or dnsmasq, thank you!

 - Download the `Unbound/90dns.conf` file from this repo, and the sample unbound.conf from the same folder.
 - Put these files into your Unbound config folder (on Linux, this is `/etc/unbound/`)
 - Tweak unbound.conf to your liking (make sure to keep the line saying `include: /etc/unbound/90dns.conf` if nothing else)
 - Start and enable unbound (`# systemctl enable --now unbound.service`)

### OpenWrt

 This part of guide is only for people who already have an OpenWrt router up and running. Please do not follow this section if you chose any of the other options.

 - Just follow the instructions in OpenWrt/README.md.
 - Optional, for people who didn't trust with wifi check bypass: edit lines with `nintendo.net` and `nintendowifi.net`, replace `95.216.149.205` with your own IP (if you intend to make the service accessible outside of your home network use your public IP, if not use your private IP)

### Mikrotik

 This part of guide is only for people who already have a Mikrotik router up and running. Please do not follow this section if you chose any of the other options.

 - Just follow the instructions in Mikrotik/README.md.
 - Optional, for people who didn't trust with wifi check bypass: edit lines with `nintendo.net` and `nintendowifi.net`, replace `95.216.149.205` with your own IP (if you intend to make the service accessible outside of your home network use your public IP, if not use your private IP)


## Web Server (only if you chose to not trust me with wifi check bypass)

#### nginx

- Download the `nginx/switchwifi` file from the repo to your `/etc/nginx/sites-enabled` folder (on non-debian based OSes you might need to set this folder yourself by messing with `/etc/nginx/nginx.conf`)
- Reload nginx (`# systemctl reload nginx`)

#### Apache2

- Download the `Apache/switchwifi.conf` file from the repo to your server.
- Download the `Apache/test` & `Apache/cdn` directories to your server.
- Update the `switchwifi.conf` file, adding the correct path for `DocumentRoot` (update `/path/to/data` with the path where you copied the previous folders) and commenting the lines depending on your Apache version.
- Update the main Apache configuration file (usually on `/etc/apache2/httpd.conf`) including the path to the previous file (example: `Include /path/to/file/switchwifi.conf`) and ensuring the needed modules are loaded (example: `LoadModule headers_module mod_headers.so`)
- Finally restart the Apache (`# systemctl reload apache2`)

### DNS (optional)

[this is the only part that I'm not too sure about, sorry. second step might not be necessary]

- Set `dns90.a3.pm.` to your own domain on all BIND9 zones
- Add an A record to that on your registrar's (or NS provider's) DNS settings, point to your DNS server

### Test the configuration

For an ideal test environment, use a device with WiFi in order to check your configuration.

- Update your device DNS configuration, adding the IP (or DNS if configured) of the server you're hosting on.
- Ping `example.nintendo.com`, it should answer `127.0.0.1`
- Open a web browser and navigate to `http://conntest.nintendowifi.net`. If you see a screen like [this](https://elixi.re/i/p6zo.png), the DNS is working as expected.

If above tests are successful, the DNS server should be correctly configured.
